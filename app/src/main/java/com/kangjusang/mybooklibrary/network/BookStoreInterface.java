package com.kangjusang.mybooklibrary.network;

import com.kangjusang.mybooklibrary.data.BookDetails;
import com.kangjusang.mybooklibrary.data.NewBooks;
import com.kangjusang.mybooklibrary.data.SearchResults;

public interface BookStoreInterface {

    void newBooks(ResponseNewBooks responseNewBooks);
    void bookDetails(String isbn13, ResponseBookDetails responseBookDetails);
    void search(String query, ResponseSearch responseSearch);
    void search(String query, int page, ResponseSearch responseSearch);

    interface ResponseNewBooks extends BaseResponse {
        void onSuccess(NewBooks newBooks);
    }

    interface ResponseBookDetails extends BaseResponse {
        void onSuccess(BookDetails bookDetails);
    }

    interface ResponseSearch extends BaseResponse {
        void onSuccess(SearchResults searchResults);
    }

    interface BaseResponse {
        void onFailure(Throwable throwable);
    }
}
