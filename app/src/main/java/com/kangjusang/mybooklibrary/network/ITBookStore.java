package com.kangjusang.mybooklibrary.network;

import android.net.Uri;

import com.kangjusang.mybooklibrary.data.BookDetails;
import com.kangjusang.mybooklibrary.data.NewBooks;
import com.kangjusang.mybooklibrary.data.SearchResults;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class ITBookStore implements BookStoreInterface {

    final static String SCHEME = "https";
    final static String AUTHORITY = "api.itbook.store";

    API api;

    public ITBookStore() {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(new Uri.Builder().scheme(SCHEME).authority(AUTHORITY).build().toString())
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
    }

    @Override
    public void newBooks(final ResponseNewBooks responseNewBooks) {
        api.newBooks().enqueue(new Callback<NewBooks>() {
            @Override
            public void onResponse(Call<NewBooks> call, Response<NewBooks> response) {
                responseNewBooks.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<NewBooks> call, Throwable t) {
                responseNewBooks.onFailure(t);
            }
        });
    }

    @Override
    public void bookDetails(String isbn13, final ResponseBookDetails responseBookDetails) {
        api.bookDetails(isbn13).enqueue(new Callback<BookDetails>() {
            @Override
            public void onResponse(Call<BookDetails> call, Response<BookDetails> response) {
                responseBookDetails.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<BookDetails> call, Throwable t) {
                responseBookDetails.onFailure(t);
            }
        });
    }

    @Override
    public void search(String query, final ResponseSearch responseSearch) {
        api.search(query).enqueue(new Callback<SearchResults>() {
            @Override
            public void onResponse(Call<SearchResults> call, Response<SearchResults> response) {
                responseSearch.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<SearchResults> call, Throwable t) {
                responseSearch.onFailure(t);
            }
        });
    }

    @Override
    public void search(String query, int page, final ResponseSearch responseSearch) {
        api.search(query, page).enqueue(new Callback<SearchResults>() {
            @Override
            public void onResponse(Call<SearchResults> call, Response<SearchResults> response) {
                responseSearch.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<SearchResults> call, Throwable t) {
                responseSearch.onFailure(t);
            }
        });
    }

    private interface API {
        @GET("/1.0/new")
        Call<NewBooks> newBooks();
        @GET("/1.0/books/{isbn13}")
        Call<BookDetails> bookDetails(@Path("isbn13") String isbn13);
        @GET("/1.0/search/{query}")
        Call<SearchResults> search(@Path("query") String query);
        @GET("/1.0/search/{query}/{page}")
        Call<SearchResults> search(@Path("query") String query, @Path("page") int page);
    }
}
