package com.kangjusang.mybooklibrary.viewmodel;

import android.widget.ArrayAdapter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.kangjusang.mybooklibrary.data.Books;

import java.util.HashMap;
import java.util.HashSet;


public class MainViewModel extends CommonViewModel {

    private MutableLiveData<ArrayAdapter<String>> mSearchQueryAdapter = new MutableLiveData<>();
    private MutableLiveData<HashMap<String,String>> mMemo = new MutableLiveData<>();
    private MutableLiveData<HashMap<String,String[]>> mBookmarks = new MutableLiveData<>();
    private MutableLiveData<HashSet<String>> mHistory = new MutableLiveData<>();

    public MainViewModel() {
        super();
        if (mMemo.getValue() == null)
            mMemo.setValue(new HashMap<String, String>());
        if (mBookmarks.getValue() == null)
            mBookmarks.setValue(new HashMap<String,String[]>());
        if (mHistory.getValue() == null)
            mHistory.setValue(new HashSet<String>());
    }

    public void setSearchQueryAdapter(ArrayAdapter<String> searchQueryAdapter) {
        mSearchQueryAdapter.setValue(searchQueryAdapter);
    }

    public LiveData<ArrayAdapter<String>> getSearchQueryAdapter() {
        return mSearchQueryAdapter;
    }

    public void updateMemo(String isbn13, String memo) {
        HashMap<String,String> stringStringHashMap = mMemo.getValue();
        stringStringHashMap.put(isbn13, memo);
        mMemo.setValue(stringStringHashMap);
    }

    public LiveData<HashMap<String, String>> getMemo() {
        return mMemo;
    }

    public void updateBookmark(String isbn13, Boolean isFavorite) {
        HashMap<String,String[]> bookmarks = mBookmarks.getValue();
        if (isFavorite) {
            HashMap<String, Books> booksHashMap = mBooks.getValue();
            Books book = booksHashMap.get(isbn13);
            String[] values = {book.getTitle(), book.getPrice(), book.getIsbn13()};
            bookmarks.put(isbn13,values);
        } else {
            bookmarks.remove(isbn13);
        }
        mBookmarks.setValue(bookmarks);
    }

    public LiveData<HashMap<String,String[]>> getBookmarks() {
        return mBookmarks;
    }

    public void addToHistory(String isbn13) {
        HashSet<String> history = mHistory.getValue();
        history.add(isbn13);
        mHistory.setValue(history);
    }

    public LiveData<HashSet<String>> getHistory() {
        return mHistory;
    }
}
