package com.kangjusang.mybooklibrary.viewmodel;

import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kangjusang.mybooklibrary.data.Books;
import com.kangjusang.mybooklibrary.data.NewBooks;
import com.kangjusang.mybooklibrary.data.SearchResults;
import com.kangjusang.mybooklibrary.network.BookStoreInterface;
import com.kangjusang.mybooklibrary.network.ITBookStore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class CommonViewModel extends ViewModel {
    public static final int FAVORITE_REQUEST = 19923;

    MutableLiveData<HashMap<String,Books>> mBooks = new MutableLiveData<>();

    private MutableLiveData<NewBooks> mNewBooks;
    private MutableLiveData<List<SearchResults>> mSearchResults = new MutableLiveData<>();


    private String mSearchQuery;

    public CommonViewModel() {

        if (mBooks.getValue() == null)
            mBooks.setValue(new HashMap<String, Books>());
        if (mSearchResults.getValue() == null)
            mSearchResults.setValue(new ArrayList<SearchResults>());
    }

    public LiveData<HashMap<String,Books>> getBooks() {
        return mBooks;
    }

    public void loadNewBooks() {
        ITBookStore itBookStore = new ITBookStore();
        itBookStore.newBooks(new BookStoreInterface.ResponseNewBooks() {
            @Override
            public void onSuccess(NewBooks newBooks) {
                mNewBooks.setValue(newBooks);
                HashMap<String, Books> books = mBooks.getValue();
                for (Books book : newBooks.getBooks()) {
                    books.put(book.getIsbn13(),book);
                }
                mBooks.setValue(books);
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d("CommonViewModel", throwable.toString());
            }
        });
    }

    public LiveData<NewBooks> getNewBooks() {
        if (mNewBooks == null) {
            mNewBooks = new MutableLiveData<>();
            loadNewBooks();
        }
        return mNewBooks;
    }

    public void search (String query) {
        mSearchQuery = query;
        ITBookStore itBookStore = new ITBookStore();
        itBookStore.search(query, new BookStoreInterface.ResponseSearch() {
            @Override
            public void onSuccess(SearchResults searchResults) {

                List<SearchResults> searchResultsList = new ArrayList<>();
                HashMap<String,Books> books = mBooks.getValue();

                String strTotal = searchResults.getTotal();
                if (!TextUtils.isEmpty(strTotal)) {
                    int total = Integer.valueOf(strTotal);
                    if (total > 0) {
                        int numOfBooksPerPage = searchResults.getBooks().length;
                        int numOfPage = total / numOfBooksPerPage + 1;
                        for (int i = 0; i < numOfPage; i++)
                            searchResultsList.add(new SearchResults());
                        searchResultsList.set(0, searchResults);

                        for (Books book : searchResults.getBooks()) {
                            books.put(book.getIsbn13(),book);
                        }
                        mBooks.setValue(books);
                    }
                }
                mSearchResults.setValue(searchResultsList);
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d("CommonViewModel", throwable.toString());
            }
        });
    }

    public void search (final int page) {
        ITBookStore itBookStore = new ITBookStore();
        itBookStore.search(mSearchQuery, page, new BookStoreInterface.ResponseSearch() {
            @Override
            public void onSuccess(SearchResults searchResults) {

                List<SearchResults> searchResultsList = mSearchResults.getValue();
                searchResultsList.set(page - 1, searchResults);
                mSearchResults.setValue(searchResultsList);

                HashMap<String,Books> books = mBooks.getValue();
                for (Books book : searchResults.getBooks()) {
                    books.put(book.getIsbn13(),book);
                }
                mBooks.setValue(books);
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d("CommonViewModel", throwable.toString());
            }
        });
    }

    public LiveData<List<SearchResults>> getSearchResults() {
        return mSearchResults;
    }

    public static List sortByArrayValue(final HashMap<String,String[]> map, final int index) {

        List<String> list = new ArrayList();
        list.addAll(map.keySet());

        Collections.sort(list,new Comparator<String>() {
            public int compare(String o1,String o2) {
                String v1 = map.get(o1)[index/2];
                String v2 = map.get(o2)[index/2];
                if (index/2 == 1 && v1.startsWith("$") && v2.startsWith("$")) {
                    float f1 = Float.valueOf(v1.substring(1));
                    float f2 = Float.valueOf(v2.substring(1));
                    return ((Comparable) f2).compareTo(f1);
                } else {
                    return ((Comparable) v2).compareTo(v1);
                }
            }
        });

        if (index%2 == 0)
            Collections.reverse(list);
        return list;
    }
}
