package com.kangjusang.mybooklibrary.viewmodel;

import android.util.Log;
import android.widget.MultiAutoCompleteTextView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.kangjusang.mybooklibrary.data.BookDetails;
import com.kangjusang.mybooklibrary.network.BookStoreInterface;
import com.kangjusang.mybooklibrary.network.ITBookStore;

import java.util.HashMap;

public class BookDetailsViewModel extends CommonViewModel {

    private MutableLiveData<BookDetails> mBookDetails = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsFavorate = new MutableLiveData<>();
    private MutableLiveData<String> mMemo = new MutableLiveData<>();

    public BookDetailsViewModel() {
        super();
    }

    public void loadBookDetails(String isbn13) {
        ITBookStore itBookStore = new ITBookStore();
        itBookStore.bookDetails(isbn13, new BookStoreInterface.ResponseBookDetails() {
            @Override
            public void onSuccess(BookDetails bookDetails) {
                mBookDetails.setValue(bookDetails);
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d("BookDetailsViewModel", throwable.toString());
            }
        });
    }

    public LiveData<BookDetails> getBookDetails() {
        return mBookDetails;
    }

    public void setIsFavorate(boolean isFavorate){
        mIsFavorate.setValue(isFavorate);
    }

    public LiveData<Boolean> getIsFavorite() {
        return mIsFavorate;
    }

    public void saveMemo(String memo) {
        mMemo.setValue(memo);
    }

    public LiveData<String> getMemo() {
        return mMemo;
    }

}
