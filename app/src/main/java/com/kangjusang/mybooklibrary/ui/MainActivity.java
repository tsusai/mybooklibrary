package com.kangjusang.mybooklibrary.ui;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.viewmodel.MainViewModel;
import com.kangjusang.mybooklibrary.ui.main.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {


    private MainViewModel mainViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
//        mainViewModel.getBookmarks().observe(this, new Observer<HashSet<String>>() {
//            @Override
//            public void onChanged(HashSet<String> strings) {
//
//            }
//        });
        
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == MainViewModel.FAVORITE_REQUEST) {
            if (resultCode == RESULT_OK) {
                String isbn13 = data.getStringExtra("isbn13");
                Boolean isFavorate = data.getBooleanExtra("isFavorite", false);
                String memo = data.getStringExtra("memo");
                mainViewModel.updateBookmark(isbn13, isFavorate);
                mainViewModel.addToHistory(isbn13);
                mainViewModel.updateMemo(isbn13, memo);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}