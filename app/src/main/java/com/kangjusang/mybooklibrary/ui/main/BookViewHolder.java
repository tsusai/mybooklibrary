package com.kangjusang.mybooklibrary.ui.main;

import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.data.Books;

public class BookViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

    ImageView ivImage;
    TextView tvTitle;
    TextView tvSubtitle;
    TextView tvPrice;
    TextView tvIsbn13;
    TextView tvUrl;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem delete = menu.add(Menu.NONE, 1001, 1, "Delete");
        delete.setOnMenuItemClickListener(onMenuItemClickListener);
    }

    interface OnBookViewClickListener {
        void onBookViewClicked(Books books);
    }

    public BookViewHolder(@NonNull final View itemView) {
        super(itemView);

        ivImage = itemView.findViewById(R.id.ivImage);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        tvSubtitle = itemView.findViewById(R.id.tvSubtitle);
        tvPrice = itemView.findViewById(R.id.tvPrice);
        tvIsbn13 = itemView.findViewById(R.id.tvIsbn13);
        tvUrl = itemView.findViewById(R.id.tvUrl);

        itemView.setOnCreateContextMenuListener(this);
    }

    public void bind(final Books books, final OnBookViewClickListener listener) {

        if (books.getImage() != null)
            Glide.with(itemView.getContext()).load(books.getImage()).into(ivImage);
        tvTitle.setText(books.getTitle());
        tvSubtitle.setText(books.getSubtitle());
        tvPrice.setText(books.getPrice());
        String isbn13 = books.getIsbn13();
        tvIsbn13.setText(isbn13 == null ? "" : "(" + isbn13 + ")");
        tvUrl.setText(books.getUrl());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBookViewClicked(books);
            }
        });
    }

    private final MenuItem.OnMenuItemClickListener onMenuItemClickListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            return false;
        }
    };
}