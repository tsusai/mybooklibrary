package com.kangjusang.mybooklibrary.ui.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.data.NewBooks;
import com.kangjusang.mybooklibrary.ui.main.BooksAdapter;
import com.kangjusang.mybooklibrary.viewmodel.MainViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class NewBooksFragment extends Fragment {

    private FragmentActivity mFragmentActivity;

    private MainViewModel mainViewModel;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFragmentActivity = getActivity();

        mainViewModel = new ViewModelProvider(this.getActivity()).get(MainViewModel.class);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_new_books, container, false);

        mSwipeRefreshLayout = root.findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mainViewModel.loadNewBooks();
            }
        });

        RecyclerView recyclerView = root.findViewById(R.id.rvNewBooks);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        final BooksAdapter booksAdapter = new BooksAdapter(mFragmentActivity);
        recyclerView.setAdapter(booksAdapter);

        mainViewModel.getNewBooks().observe(this.getViewLifecycleOwner(), new Observer<NewBooks>() {
            @Override
            public void onChanged(NewBooks newBooks) {

                booksAdapter.updateItem(new ArrayList<>(Arrays.asList(newBooks.getBooks())));
                booksAdapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mainViewModel.getBookmarks().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String,String[]>>() {
            @Override
            public void onChanged(HashMap<String,String[]> strings) {
                booksAdapter.updateBookmarks(strings);
                booksAdapter.notifyDataSetChanged();
            }
        });

        mainViewModel.getMemo().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String, String>>() {
            @Override
            public void onChanged(HashMap<String, String> stringStringHashMap) {
                booksAdapter.updateMemos(stringStringHashMap);
                booksAdapter.notifyDataSetChanged();
            }
        });

        return root;
    }
}
