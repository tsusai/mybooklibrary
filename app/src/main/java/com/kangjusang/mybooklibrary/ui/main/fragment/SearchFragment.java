package com.kangjusang.mybooklibrary.ui.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.data.SearchResults;
import com.kangjusang.mybooklibrary.viewmodel.MainViewModel;
import com.kangjusang.mybooklibrary.ui.main.SearchResultsPageAdapter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class SearchFragment extends Fragment {

    private FragmentActivity mFragmentActivity;

    private MainViewModel mainViewModel;

    SearchView.SearchAutoComplete mSearchAutoComplete;
    ArrayAdapter<String> mQueryHistory;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.search);

        if (item != null) {
            final SearchView searchView = (SearchView) item.getActionView();
            if (searchView != null) {
                searchView.setSubmitButtonEnabled(true);

                mSearchAutoComplete = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
                mSearchAutoComplete.setDropDownBackgroundResource(android.R.color.white);
                if (mQueryHistory == null) {
                    mQueryHistory = new ArrayAdapter<>(mFragmentActivity, android.R.layout.simple_dropdown_item_1line);
                }
                mSearchAutoComplete.setAdapter(mQueryHistory);
                mSearchAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String query = (String) parent.getItemAtPosition(position);
                        mSearchAutoComplete.setText(query);
                        mainViewModel.search(query);
                    }
                });

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        if (mQueryHistory.getPosition(query) == -1) {
                            mQueryHistory.add(query);
                            mainViewModel.setSearchQueryAdapter(mQueryHistory);
                        }
                        mainViewModel.search(query);
                        searchView.clearFocus();
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });
                searchView.clearFocus();
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFragmentActivity = getActivity();

        mainViewModel = new ViewModelProvider(this.getActivity()).get(MainViewModel.class);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);

        mSwipeRefreshLayout = root.findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                String query = mSearchAutoComplete.getText().toString();
                if (mQueryHistory.getPosition(query) == -1) {
                    mQueryHistory.add(query);
                    mainViewModel.setSearchQueryAdapter(mQueryHistory);
                }
                mainViewModel.search(query);
            }
        });

        RecyclerView recyclerView = root.findViewById(R.id.rvSearchResultsPage);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        final SearchResultsPageAdapter searchResultsPageAdapter = new SearchResultsPageAdapter(mFragmentActivity, mainViewModel);
        recyclerView.setAdapter(searchResultsPageAdapter);

        mainViewModel.getSearchResults().observe(this.getViewLifecycleOwner(), new Observer<List<SearchResults>>() {
            @Override
            public void onChanged(List<SearchResults> searchResults) {
                searchResultsPageAdapter.updateItem(searchResults);
                searchResultsPageAdapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mainViewModel.getBookmarks().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String,String[]>>() {
            @Override
            public void onChanged(HashMap<String,String[]> strings) {
                searchResultsPageAdapter.updateBookmarks(strings);
                searchResultsPageAdapter.notifyDataSetChanged();
            }
        });

        mainViewModel.getMemo().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String, String>>() {
            @Override
            public void onChanged(HashMap<String, String> stringStringHashMap) {
                searchResultsPageAdapter.updateMemos(stringStringHashMap);
                searchResultsPageAdapter.notifyDataSetChanged();
            }
        });

        mainViewModel.getSearchQueryAdapter().observe(this.getViewLifecycleOwner(), new Observer<ArrayAdapter<String>>() {
            @Override
            public void onChanged(ArrayAdapter<String> stringArrayAdapter) {
                mQueryHistory = stringArrayAdapter;
                if (mSearchAutoComplete != null) {
                    mSearchAutoComplete.setAdapter(mQueryHistory);
                }
            }
        });

        return root;
    }
}
