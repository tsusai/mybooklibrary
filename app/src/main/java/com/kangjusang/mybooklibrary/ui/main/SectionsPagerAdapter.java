package com.kangjusang.mybooklibrary.ui.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.ui.main.fragment.BookmarkFragment;
import com.kangjusang.mybooklibrary.ui.main.fragment.HistoryFragment;
import com.kangjusang.mybooklibrary.ui.main.fragment.NewBooksFragment;
import com.kangjusang.mybooklibrary.ui.main.fragment.SearchFragment;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3, R.string.tab_text_4};
    private static final Class[] TAB_CLASSES = new Class[]{ NewBooksFragment.class,
                                                            SearchFragment.class,
                                                            BookmarkFragment.class,
                                                            HistoryFragment.class};

    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Object instance = null;
        try {
            instance = TAB_CLASSES[position].newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return (Fragment)instance;
//        return PlaceholderFragment.newInstance(position + 1);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return TAB_TITLES.length;
    }
}