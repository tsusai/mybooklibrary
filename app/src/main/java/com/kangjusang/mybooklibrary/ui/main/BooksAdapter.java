package com.kangjusang.mybooklibrary.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.data.Books;
import com.kangjusang.mybooklibrary.ui.BookDetailsActivity;
import com.kangjusang.mybooklibrary.viewmodel.MainViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class BooksAdapter extends RecyclerView.Adapter<BookViewHolder> {

    Activity mActivity;
    List<Books> mBooks = new ArrayList<>();
    HashMap<String,String[]> mBookmarks = new HashMap<>();
    HashMap<String, String> mMemos = new HashMap<>();

    public BooksAdapter(Activity activity) {
        mActivity = activity;
    }

    public void updateItem(List<Books> books) {
        mBooks = books;
    }

    public void updateBookmarks(HashMap<String,String[]> bookmarks) {
        mBookmarks = bookmarks;
    }

    public void updateMemos(HashMap<String, String> memos) {
        mMemos = memos;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.book, parent, false);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {

//        Log.d("SearchResultsPageAdp", "\tonBindViewHolder : " + position);
        Books books = mBooks.get(position);
        holder.bind(books, new BookViewHolder.OnBookViewClickListener() {
            @Override
            public void onBookViewClicked(Books books) {
                Intent intent = new Intent(mActivity, BookDetailsActivity.class);
                intent.putExtra("isbn13", books.getIsbn13());
                intent.putExtra("isFavorite", mBookmarks.keySet().contains(books.getIsbn13()));
                intent.putExtra("memo", mMemos.get(books.getIsbn13()));
                mActivity.startActivityForResult(intent, MainViewModel.FAVORITE_REQUEST);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBooks.size();
    }

}
