package com.kangjusang.mybooklibrary.ui.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.data.Books;
import com.kangjusang.mybooklibrary.ui.main.BooksAdapter;
import com.kangjusang.mybooklibrary.viewmodel.MainViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BookmarkFragment extends Fragment {

    private FragmentActivity mFragmentActivity;

    private MainViewModel mainViewModel;

    private HashMap<String,Books> mBooks;
    private HashMap<String,String[]> mBookmarks;

    Spinner mSpinner;
    BooksAdapter mBooksAdapter;

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {


        inflater.inflate(R.menu.menu_spinner, menu);

        MenuItem item = menu.findItem(R.id.spinner);
        mSpinner = (Spinner) item.getActionView();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mFragmentActivity, R.array.sort_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<String> keys = MainViewModel.sortByArrayValue(mBookmarks,mSpinner.getSelectedItemPosition());
                List<Books> books = new ArrayList<>();
                for (String isbn13 : keys) {
                    Books book = mBooks.get(isbn13);
                    if (book != null)
                        books.add(book);
                }
                mBooksAdapter.updateItem(books);
                mBooksAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mainViewModel.getBookmarks().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String,String[]>>() {
            @Override
            public void onChanged(HashMap<String,String[]> strings) {

                mBookmarks = strings;
                List<String> keys = MainViewModel.sortByArrayValue(strings,mSpinner.getSelectedItemPosition());
                List<Books> books = new ArrayList<>();
                for (String isbn13 : keys) {
                    Books book = mBooks.get(isbn13);
                    if (book != null)
                        books.add(book);
                }
                mBooksAdapter.updateItem(books);
                mBooksAdapter.updateBookmarks(strings);
                mBooksAdapter.notifyDataSetChanged();
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFragmentActivity = getActivity();
        mainViewModel = new ViewModelProvider(this.getActivity()).get(MainViewModel.class);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_bookmarks, container, false);

        mBooks = new HashMap<>();
//        mBookmarks = new HashMap<>();

        RecyclerView recyclerView = root.findViewById(R.id.rvBookmarks);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        mBooksAdapter = new BooksAdapter(mFragmentActivity);
        recyclerView.setAdapter(mBooksAdapter);

        mainViewModel.getBooks().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String, Books>>() {
            @Override
            public void onChanged(HashMap<String, Books> stringBooksHashMap) {
                mBooks.clear();
                mBooks.putAll(stringBooksHashMap);
            }
        });



        mainViewModel.getMemo().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String, String>>() {
            @Override
            public void onChanged(HashMap<String, String> stringStringHashMap) {
                mBooksAdapter.updateMemos(stringStringHashMap);
                mBooksAdapter.notifyDataSetChanged();
            }
        });

        return root;
    }
}
