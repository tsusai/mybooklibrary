package com.kangjusang.mybooklibrary.ui.main;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.data.Books;
import com.kangjusang.mybooklibrary.data.SearchResults;
import com.kangjusang.mybooklibrary.viewmodel.MainViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class SearchResultsPageAdapter extends RecyclerView.Adapter<SearchResultsPageAdapter.ViewHolder> {

    Activity mActivity;
    MainViewModel mMainViewModel;

    private List<SearchResults> mSearchResults = new ArrayList<>();
    HashMap<String,String[]> mBookmarks = new HashMap<>();
    HashMap<String,String> mMemos = new HashMap<>();

    RecyclerView.RecycledViewPool mViewPool;

    public SearchResultsPageAdapter(Activity activity, MainViewModel mainViewModel) {
        mActivity = activity;
        mMainViewModel = mainViewModel;
        mViewPool = new RecyclerView.RecycledViewPool();
    }

    public void updateItem(List<SearchResults> searchResults) {
        mSearchResults = searchResults;
    }

    public void updateBookmarks(HashMap<String,String[]> bookmarks) {
        mBookmarks = bookmarks;
    }

    public void updateMemos(HashMap<String,String> memos) {
        mMemos = memos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.search_results_page, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        BooksAdapter booksAdapter = new BooksAdapter(mActivity);
        viewHolder.rvSearchResults.setRecycledViewPool(mViewPool);
        viewHolder.rvSearchResults.setAdapter(booksAdapter);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Log.d("SearchResultsPageAdp", "onBindViewHolder : " + position);
        SearchResults searchResults = mSearchResults.get(position);
//        holder.tvTemp.setText("" + (position+1));
        BooksAdapter booksAdapter = (BooksAdapter) holder.rvSearchResults.getAdapter();
        if (searchResults.getBooks() == null) {
            // Todo : dummy arrays' size to be dynamic
            Books[] dummyBooks = new Books[10];
            Arrays.fill(dummyBooks, new Books());
            booksAdapter.updateItem(new ArrayList(Arrays.asList(dummyBooks)));
            booksAdapter.updateBookmarks(mBookmarks);
            booksAdapter.updateMemos(mMemos);
            booksAdapter.notifyDataSetChanged();
            mMainViewModel.search(position + 1);
        }
        else {
            booksAdapter.updateItem(new ArrayList(Arrays.asList(searchResults.getBooks())));
            booksAdapter.updateBookmarks(mBookmarks);
            booksAdapter.updateMemos(mMemos);
            booksAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return mSearchResults.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public RecyclerView rvSearchResults;
//        public TextView tvTemp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            rvSearchResults = itemView.findViewById(R.id.rvSearchResults);
            rvSearchResults.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
//            tvTemp = itemView.findViewById(R.id.tvTemp);
        }
    }
}
