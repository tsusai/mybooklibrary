package com.kangjusang.mybooklibrary.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.data.BookDetails;
import com.kangjusang.mybooklibrary.viewmodel.BookDetailsViewModel;
import com.kangjusang.mybooklibrary.viewmodel.MainViewModel;

public class BookDetailsActivity extends AppCompatActivity {

    private BookDetailsViewModel bookDetailsViewModel;
    private Activity mActivity;
    private MenuItem mFavorite;
    private String mIsbn13;

    private String mMemo;
    private Boolean mIsFavorite;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Intent intent = getIntent();

        boolean isFavorite = intent.getBooleanExtra("isFavorite", false);
        if (mIsFavorite != null) {
            isFavorite = mIsFavorite;
        }
        if (mMemo == null)
            mMemo = intent.getStringExtra("memo");

        getMenuInflater().inflate(R.menu.menu_book_details, menu);

        mFavorite = menu.findItem(R.id.miFavorite);
        if (isFavorite)
            mFavorite.setIcon(android.R.drawable.btn_star_big_on);
        else
            mFavorite.setIcon(android.R.drawable.btn_star_big_off);
        mFavorite.setChecked(isFavorite);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.miFavorite:
                boolean isChecked = !(item.isChecked());
                if (isChecked)
                    item.setIcon(android.R.drawable.btn_star_big_on);
                else
                    item.setIcon(android.R.drawable.btn_star_big_off);
                item.setChecked(isChecked);
                bookDetailsViewModel.setIsFavorate(isChecked);
                return true;

            case R.id.miMemo:
                editMemo();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void editMemo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("User's memo");
        builder.setMessage("Write what you want to save");
        final EditText et = new EditText(this);
        et.setText(mMemo, TextView.BufferType.EDITABLE);
        builder.setView(et);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                bookDetailsViewModel.saveMemo(et.getText().toString());
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra("isFavorite", mFavorite.isChecked());
        intent.putExtra("memo", mMemo);
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        mActivity = this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        mIsbn13 = intent.getStringExtra("isbn13");

        bookDetailsViewModel = new ViewModelProvider(this).get(BookDetailsViewModel.class);

        final TextView tvTitle = findViewById(R.id.tvTitle);
        final TextView tvSubtitle = findViewById(R.id.tvSubtitle);
        final ImageView ivImage = findViewById(R.id.ivImage);
        final TextView tvPrice = findViewById(R.id.tvPrice);
        final RatingBar rbRating = findViewById(R.id.rbRating);
        final TextView tvAuthors = findViewById(R.id.tvAuthors);
        final TextView tvPublisher = findViewById(R.id.tvPublisher);
        final TextView tvYear = findViewById(R.id.tvYear);
        final TextView tvPages = findViewById(R.id.tvPages);
        final TextView tvLanguage = findViewById(R.id.tvLanguage);
        final TextView tvIsbn10 = findViewById(R.id.tvIsbn10);
        final TextView tvIsbn13 = findViewById(R.id.tvIsbn13);
        final TextView tvUrl = findViewById(R.id.tvUrl);
        final TextView tvDesc = findViewById(R.id.tvDesc);
        final TableRow trMemoTitle = findViewById(R.id.trMemoTitle);
        final TableRow trMemo = findViewById(R.id.trMemo);
        final TextView tvMemo = findViewById(R.id.tvMemo);

        bookDetailsViewModel.getBookDetails().observe(this, new Observer<BookDetails>() {
            @Override
            public void onChanged(BookDetails bookDetails) {
                Glide.with(mActivity).load(bookDetails.getImage()).into(ivImage);
                tvTitle.setText(bookDetails.getTitle());
                tvSubtitle.setText(bookDetails.getSubtitle());
                tvPrice.setText(bookDetails.getPrice());
                Float rating = Float.valueOf(bookDetails.getRating());
                rbRating.setRating(rating);
                rbRating.setVisibility(View.VISIBLE);
                tvAuthors.setText(bookDetails.getAuthors());
                tvPublisher.setText(bookDetails.getPublisher());
                tvYear.setText(bookDetails.getYear());
                tvPages.setText(bookDetails.getPages());
                tvLanguage.setText(bookDetails.getLanguage());
                tvIsbn10.setText(bookDetails.getIsbn10());
                tvIsbn13.setText(bookDetails.getIsbn13());
                final String url = bookDetails.getUrl();
                tvUrl.setText(url);
                tvUrl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        mActivity.startActivity(intent);
                    }
                });
                tvDesc.setText(bookDetails.getDesc());
            }
        });
        bookDetailsViewModel.loadBookDetails(mIsbn13);

        bookDetailsViewModel.getMemo().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                mMemo = s;
                if (!TextUtils.isEmpty(mMemo)) {
                    trMemoTitle.setVisibility(View.VISIBLE);
                    trMemo.setVisibility(View.VISIBLE);
                    tvMemo.setText(mMemo);
                } else {
                    trMemoTitle.setVisibility(View.GONE);
                    trMemo.setVisibility(View.GONE);
                    tvMemo.setText(mMemo);
                }
            }
        });

        bookDetailsViewModel.getIsFavorite().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                mIsFavorite = aBoolean;
            }
        });
    }
}
