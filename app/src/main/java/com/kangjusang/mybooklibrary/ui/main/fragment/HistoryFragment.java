package com.kangjusang.mybooklibrary.ui.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kangjusang.mybooklibrary.R;
import com.kangjusang.mybooklibrary.data.Books;
import com.kangjusang.mybooklibrary.ui.main.BooksAdapter;
import com.kangjusang.mybooklibrary.viewmodel.MainViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class HistoryFragment extends Fragment {

    private FragmentActivity mFragmentActivity;

    private MainViewModel mainViewModel;

    private HashMap<String,Books> mBooks;
    private HashSet<String> mHistory;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFragmentActivity = getActivity();

        mainViewModel = new ViewModelProvider(this.getActivity()).get(MainViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_history, container, false);

        mBooks = new HashMap<>();
        mHistory = new HashSet<>();

        RecyclerView recyclerView = root.findViewById(R.id.rvHistory);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        final BooksAdapter booksAdapter = new BooksAdapter(mFragmentActivity);
        recyclerView.setAdapter(booksAdapter);

        mainViewModel.getBooks().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String, Books>>() {
            @Override
            public void onChanged(HashMap<String, Books> stringBooksHashMap) {
                mBooks.clear();
                mBooks.putAll(stringBooksHashMap);
            }
        });

        mainViewModel.getHistory().observe(this.getViewLifecycleOwner(), new Observer<HashSet<String>>() {
            @Override
            public void onChanged(HashSet<String> strings) {
                mHistory.clear();
                mHistory.addAll(strings);
                Iterator iterator = strings.iterator();
                List<Books> books = new ArrayList<>();
                while(iterator.hasNext()) {
                    String isbn13 = (String) iterator.next();
                    books.add(mBooks.get(isbn13));
                }
                booksAdapter.updateItem(books);
                booksAdapter.notifyDataSetChanged();
            }
        });

        mainViewModel.getBookmarks().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String,String[]>>() {
            @Override
            public void onChanged(HashMap<String,String[]> strings) {
                booksAdapter.updateBookmarks(strings);
                booksAdapter.notifyDataSetChanged();
            }
        });

        mainViewModel.getMemo().observe(this.getViewLifecycleOwner(), new Observer<HashMap<String, String>>() {
            @Override
            public void onChanged(HashMap<String, String> stringStringHashMap) {
                booksAdapter.updateMemos(stringStringHashMap);
                booksAdapter.notifyDataSetChanged();
            }
        });

        return root;
    }
}
