package com.kangjusang.mybooklibrary.data;

public class BookDetails
{
    private String image;

    private String year;

    private String rating;

    private String language;

    private String error;

    private String title;

    private String url;

    private String pages;

    private String price;

    private String subtitle;

    private String isbn13;

    private String publisher;

    private String isbn10;

    private String authors;

    private String desc;

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    public String getYear ()
    {
        return year;
    }

    public void setYear (String year)
    {
        this.year = year;
    }

    public String getRating ()
    {
        return rating;
    }

    public void setRating (String rating)
    {
        this.rating = rating;
    }

    public String getLanguage ()
    {
        return language;
    }

    public void setLanguage (String language)
    {
        this.language = language;
    }

    public String getError ()
    {
        return error;
    }

    public void setError (String error)
    {
        this.error = error;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    public String getPages ()
    {
        return pages;
    }

    public void setPages (String pages)
    {
        this.pages = pages;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getSubtitle ()
    {
        return subtitle;
    }

    public void setSubtitle (String subtitle)
    {
        this.subtitle = subtitle;
    }

    public String getIsbn13 ()
    {
        return isbn13;
    }

    public void setIsbn13 (String isbn13)
    {
        this.isbn13 = isbn13;
    }

    public String getPublisher ()
    {
        return publisher;
    }

    public void setPublisher (String publisher)
    {
        this.publisher = publisher;
    }

    public String getIsbn10 ()
    {
        return isbn10;
    }

    public void setIsbn10 (String isbn10)
    {
        this.isbn10 = isbn10;
    }

    public String getAuthors ()
    {
        return authors;
    }

    public void setAuthors (String authors)
    {
        this.authors = authors;
    }

    public String getDesc ()
    {
        return desc;
    }

    public void setDesc (String desc)
    {
        this.desc = desc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [image = "+image+", year = "+year+", rating = "+rating+", language = "+language+", error = "+error+", title = "+title+", url = "+url+", pages = "+pages+", price = "+price+", subtitle = "+subtitle+", isbn13 = "+isbn13+", publisher = "+publisher+", isbn10 = "+isbn10+", authors = "+authors+", desc = "+desc+"]";
    }
}
