package com.kangjusang.mybooklibrary.data;

public class SearchResults
{
    private String total;

    private Books[] books;

    private String page;

    private String error;

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public Books[] getBooks ()
    {
        return books;
    }

    public void setBooks (Books[] books)
    {
        this.books = books;
    }

    public String getPage ()
    {
        return page;
    }

    public void setPage (String page)
    {
        this.page = page;
    }

    public String getError ()
    {
        return error;
    }

    public void setError (String error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [total = "+total+", books = "+books+", page = "+page+", error = "+error+"]";
    }
}
