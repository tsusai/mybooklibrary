package com.kangjusang.mybooklibrary.data;

public class NewBooks
{
    private String total;

    private Books[] books;

    private String error;

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public Books[] getBooks ()
    {
        return books;
    }

    public void setBooks (Books[] books)
    {
        this.books = books;
    }

    public String getError ()
    {
        return error;
    }

    public void setError (String error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [total = "+total+", books = "+books+", error = "+error+"]";
    }
}
